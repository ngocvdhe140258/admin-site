<%-- 
    Document   : coursecrud
    Created on : May 21, 2023, 12:29:30 AM
    Author     : doanq
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>DASHMIN - Bootstrap Admin Template</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="keywords">
        <meta content="" name="description">
        
        <style>
    /* Ẩn thông báo "Không có tệp nào được chọn" */
    .file-input {
        overflow: hidden;
        position: relative;
    }

    .file-input input[type="file"] {
        font-size: 100px;
        position: absolute;
        left: 0;
        top: 0;
        opacity: 0;
    }
</style>
        
    </head>
    <body>
        
        <form action="updateprofile" method="post">
            <input type="text" name="account_email" value="${mode eq '1'? email : "khong co"}">
            ${mess}
            <input type="submit" value="UPDATE">
        </form>
    </body>
</html>